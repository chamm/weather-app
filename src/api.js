const WeatherAPI = (key, location) => {
  let state = {};

  const parse = (json) => {
    state.current = {
      temp: Math.round(json.current.temp) + "°",
      humidity: json.current.humidity,
      feelsLike: Math.round(json.current.feels_like) + "°",
      condition: json.current.weather[0].description,
      clouds: json.current.clouds,
      rain: Math.round(json.daily[0].rain) + "mm",
      icon:
        "https://openweathermap.org/img/wn/" +
        json.current.weather[0].icon +
        "@2x.png",
    };

    json.daily.forEach((forecast, index) => {
      state.daily[index] = {
        temp: forecast.temp.day,
        humidity: forecast.humidity,
        feelsLike: forecast.feels_like.day,
        condition: forecast.weather[0].description,
        clouds: forecast.clouds,
        rain: forecast.rain === undefined ? 0 : forecast.rain,
      };
    });
  };

  const update = async () => {
    const req = `https://api.openweathermap.org/data/2.5/onecall?lat=${location.lat}&lon=${location.lon}&units=metric&exclude=minutely,hourly,alerts&appid=${key}`;
    const res = await fetch(req, { mode: "cors" });
    const json = await res.json();

    parse(json);
  };

  const init = async () => {
    state = {
      current: {},
      daily: [],
      city: "",
    };

    await update();

    const req = `http://api.openweathermap.org/geo/1.0/reverse?lat=${location.lat}&lon=${location.lon}&limit=1&appid=${key}`;
    const res = await fetch(req, { mode: "cors" });
    const json = await res.json();

    state.city = json[0].name + ", " + json[0].country;
  };

  const getCity = () => state.city;

  const getDailyForecast = (day) => {
    if (day < 0 || day > 7) return;

    return state.daily[day];
  };

  const getCurrentForecast = () => {
    return state.current;
  };

  return { init, update, getDailyForecast, getCurrentForecast, getCity };
};

export default WeatherAPI;
