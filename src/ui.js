const UI = (container) => {
  let state = {};

  const updateCurrentForecast = (forecast, location, time) => {
    state.currentElements["current-location"].textContent = location;
    state.currentElements["current-time"].textContent = time;
    state.currentElements["current-temperature"].textContent = forecast.temp;
    state.currentElements["current-feels"].textContent =
      "Feels like: " + forecast.feelsLike;
    state.currentElements["current-conditions"].textContent =
      forecast.condition;
    state.currentElements["current-rain"].textContent =
      "Rain: " + forecast.rain;

    state.currentIcon.src = forecast.icon;
    state.currentIcon.alt = forecast.condition;
  };

  const init = () => {
    state.current = document.createElement("div");
    state.currentText = document.createElement("div");
    state.currentIcon = document.createElement("img");

    state.current.id = "current-forecast";
    state.currentText.id = "current-forecast-info";
    state.currentIcon.id = "current-forecast-icon";

    state.current.append(state.currentText, state.currentIcon);

    state.daily = document.createElement("div");

    state.daily.id = "daily-forecast";

    state.currentElements = {};
    const currentElements = [
      "current-location",
      "current-time",
      "current-temperature",
      "current-feels",
      "current-conditions",
      "current-rain",
    ];

    currentElements.forEach((id) => {
      const element = document.createElement("div");

      element.classList.add("t-forecast");
      element.id = id;
      state.currentElements[id] = element;
      state.currentText.append(element);
    });

    const dailyElements = [];

    container.append(state.current, state.daily);
  };

  return { init, updateCurrentForecast };
};

export default UI;
