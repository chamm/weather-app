import App from "./app.js";

import "core-js/stable";
import "regenerator-runtime/runtime";

import "./style.css";

const container = document.createElement("div");
container.classList.add("container");
document.body.append(container);

const app = new App(container);
