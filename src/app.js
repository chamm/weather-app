import UI from "./ui.js";
import WeatherAPI from "./api.js";

async function getLocation() {
  const options = {
    enableHighAccuracy: true,
    timeout: 10000,
    maximumAge: 0,
  };

  const result = await new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(resolve, reject, options);
  });

  return { lat: result.coords.latitude, lon: result.coords.longitude };
}

const App = (container) => {
  const ui = new UI(container);
  const today = new Date();
  let time = `${today.getHours()}:${today.getMinutes()}`;

  ui.init();

  getLocation().then((res) => {
    const api = WeatherAPI(process.env.API_KEY, res);

    api.init().then(() => {
      const forecast = api.getCurrentForecast();
      const city = api.getCity();

      ui.updateCurrentForecast(forecast, city, time);
    });
  });
};

export default App;
