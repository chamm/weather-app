# weather-app

A simple app that fetches local weather forecasts from OpenWeatherMap

### Setup Instructions

- OpenWeather requires an API key, see: https://openweathermap.org/api
- Once you have a key, create a `.env` file in the root of the project directory
- Configure your API key inside the file:

`API_KEY=key_goes_here`

### Running the App

- Install project dependencies: `npm install`
- Run the app: `npm run start`
